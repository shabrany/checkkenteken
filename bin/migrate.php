<?php

$config = require __DIR__.'/../config.php';
$migrationsDirectory = __DIR__.'/../database/migrations';
$revisionFile = __DIR__.'/../database/.migrated';

function getPdo($host, $dbname, $user, $password)
{
    $dns = "mysql:host={$host};dbname=${dbname}";
    $pdo = new PDO($dns, $user, $password);

    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

    return $pdo;
}

function getMigratedFiles($revisionFile)
{
    if (!file_exists($revisionFile)) {
        touch($revisionFile);
    }

    $migrations = file($revisionFile);

    return array_map('trim', $migrations);
}

function printLine($message)
{
    echo $message.PHP_EOL;
}

$revision = array_merge(['.', '..'], getMigratedFiles($revisionFile));
$migrations = array_diff(scandir($migrationsDirectory), $revision);

if (!count($migrations)) {
    printLine('Nothing to migrate...');
    exit(0);
}

$db = $config['db'];

$pdo = getPdo($db['host'], $db['dbname'], $db['user'], $db['pass']);

$pdo->beginTransaction();

$migrationsDone = [];

foreach ($migrations as $migration) {
    $absolutePathMigrationFile = $migrationsDirectory.'/'.$migration;
    $sql = file_get_contents($absolutePathMigrationFile);

    try {
        $pdo->exec($sql);
        $migrationsDone[] = $migration;
        printLine("Migrated: $migration");
    } catch (Exception $e) {
        $pdo->rollBack();
        printLine($e->getMessage());
        exit(0);
    }
}

if (count($migrationsDone)) {
    file_put_contents($revisionFile, implode("\n", $migrationsDone), FILE_APPEND);
}

$pdo->commit();