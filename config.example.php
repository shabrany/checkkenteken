<?php

return [
    'settings' => [
        'displayErrorDetails' => true
    ],
    'db' => [
        'host' => 'localhost',
        'user' => 'root',
        'pass' => 'root',
        'dbname' => 'kenteken',
    ]
];