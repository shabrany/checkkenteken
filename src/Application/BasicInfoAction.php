<?php

namespace shabrany\Application;

use shabrany\Domain\CarInfoService;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\PhpRenderer;

class BasicInfoAction
{
    /** @var CarInfoService */
    private $carInfoService;

    private $view;

    public function __construct(CarInfoService $carInfoService, PhpRenderer $view)
    {
        $this->carInfoService = $carInfoService;
        $this->view = $view;
    }

    public function __invoke(Request $request, Response $response, array $args)
    {
        $post = $request->getParsedBody();
        $basicData = $this->carInfoService->getBasicInfoByLicensePlate($post['license-plate']);
        $this->view->render($response, 'basic-info.php', ['vehicle' => $basicData]);
    }
}
