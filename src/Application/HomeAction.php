<?php

namespace shabrany\Application;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Views\PhpRenderer;

class HomeAction {

    private $view;

    public function __construct(PhpRenderer $view)
    {
        $this->view = $view;
    }

    public function __invoke(RequestInterface $request, ResponseInterface $repsonse, array $args)
    {
        return $this->view->render($repsonse, 'home.php');
    }
}
