<?php 

namespace shabrany\Domain;

class LicensePlate {

    /** @var string */
    private $licensePlate;
    
    public function __construct(string $licensePlate)
    {
        $this->licensePlate = $licensePlate;
    }

    public function get(): string
    {
        return $this->licensePlate;
    }
}