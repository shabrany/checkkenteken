<?php

namespace shabrany\Domain;

use shabrany\Infrastructure\DatabaseAccessor;

class CarRepository {

    /** @var DatabaseAccessor */
    private $dba;

    public function __construct(DatabaseAccessor $dba)
    {
        $this->dba = $dba;
    }

    public function getBasicInfo(LicensePlate $licensePlate)
    {
        return $this->dba->query("SELECT * FROM vehicles", []);
    }
}