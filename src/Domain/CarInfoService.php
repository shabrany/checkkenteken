<?php

namespace shabrany\Domain;

use shabrany\Infrastructure\RdwApiService;

class CarInfoService
{
    /** @var CarRepository */
    private $carRepository;

    private $rdwApiService;

    public function __construct(CarRepository $carRepository, RdwApiService $rdwApiService)
    {
        $this->carRepository = $carRepository;
        $this->rdwApiService = $rdwApiService;
    }

    public function getBasicInfoByLicensePlate($licensePlate)
    {
        return $this->rdwApiService->getVehicle($licensePlate);
    }
}
