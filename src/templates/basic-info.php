<dl>
    <dt>Kenteken</dt>
    <dd><?= $vehicle->kenteken ?></dd>

    <dt>Merk</dt>
    <dd><?= $vehicle->merk ?></dd>

    <dt>Model</dt>
    <dd><?= $vehicle->handelsbenaming ?></dd>

    <dt>Voertuigsoort</dt>
    <dd><?= $vehicle->voertuigsoort ?></dd>

    <dt>Inrichting</dt>
    <dd><?= $vehicle->inrichting ?></dd>

    <dt>Bouwjaar</dt>
    <dd><?= DateTime::createFromFormat('Ymd', $vehicle->datum_eerste_toelating)->format('Y-m-d') ?></dd>
</dl>