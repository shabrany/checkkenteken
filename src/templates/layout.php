<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Checkkenteken.nl</title>
    <link rel="stylesheet" href="dist/main.css">
</head>
<body class="container pt3 pb3">
    <div id="app" class=""><?= $content ?></div>
    <script src="dist/main.js"></script>
</body>
</html>