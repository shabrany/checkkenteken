<?php

use shabrany\Application\HomeAction;
use shabrany\Application\BasicInfoAction;
use shabrany\Infrastructure\DatabaseAccessor;
use shabrany\Domain\CarInfoService;
use shabrany\Domain\CarRepository;
use shabrany\Infrastructure\RdwApiService;

$config = require __DIR__ . '/../config.php';

$container = new \Slim\Container($config);

$container['view'] = function () {
    return new \Slim\Views\PhpRenderer(__DIR__ . '/templates', [], 'layout.php');
};

$container['conn'] = function ($container) {
    $db = $container['db'];
    return new DatabaseAccessor($db['host'], $db['dbname'], $db['user'], $db['pass']);
};

$container['carInfoService'] = function ($container) {
    $carRepository = new CarRepository($container['conn']);
    return new CarInfoService($carRepository, $container['rdwApiService']);
};

$container[HomeAction::class] = function ($container) {
    return new HomeAction($container['view']);
};

$container[BasicInfoAction::class] = function ($container) {
    return new BasicInfoAction($container['carInfoService'], $container['view']);
};

$container['rdwApiService'] = function ($container) {
    return new RdwApiService(
        $container['rdw_api']['api_url'],
        $container['rdw_api']['app_token']
    );
};

return $container;
