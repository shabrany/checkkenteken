<?php
namespace shabrany\Infrastructure;

use GuzzleHttp\Client;

class RdwApiService {

    private $httpClient;

    private const RESOURCE_VEHICLE = '/resource/m9d7-ebf2.json';

    public function __construct($apiUrl, $apiToken)
    {
        $this->httpClient = new Client([
            'base_uri' => $apiUrl,
            'headers' => ['X-App-Token' => $apiToken]
        ]);
    }

    public function getVehicle($licensePlate)
    {
        $response = $this->httpClient->get(self::RESOURCE_VEHICLE, [
            'query' => ['kenteken' => $licensePlate]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if (count($contents) == 0) {
           return false;
        }

        return $contents[0];
    }
}