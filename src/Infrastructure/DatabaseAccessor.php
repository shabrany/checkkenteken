<?php

namespace shabrany\Infrastructure;

use PDO;

class DatabaseAccessor
{
    /** @var PDO  */
    private $pdo;

    public function __construct($host, $name, $user, $pass)
    {
        $this->pdo = new PDO("mysql:host=$host;dbname=$name", $user, $pass);
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        $this->pdo->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, 'SET NAMES utf8');
    }

    /**
     * @return PDO
     */
    public function get()
    {
        return $this->pdo;
    }

    public function query(string $query, array $parameters): array
    {
        $stmt = $this->pdo->prepare($query);
        $stmt->execute($parameters);
        return $stmt->fetchAll();
    }
}