export default {
    getSidecode(licensePlate) {

        let sidecodes = [];
        //except licenseplates for diplomats
        let specialLicensePlates = '^CD[ABFJNST][0-9]{1,3}$';

        licensePlate = licensePlate.replace(/-/g, '');

        sidecodes.push(/^[a-zA-Z]{2}[\d]{2}[\d]{2}$/);         //   1       XX-99-99
        sidecodes.push(/^[\d]{2}[\d]{2}[a-zA-Z]{2}$/);         //   2       99-99-XX
        sidecodes.push(/^[\d]{2}[a-zA-Z]{2}[\d]{2}$/);         //   3       99-XX-99
        sidecodes.push(/^[a-zA-Z]{2}[\d]{2}[a-zA-Z]{2}$/);     //   4       XX-99-XX
        sidecodes.push(/^[a-zA-Z]{2}[a-zA-Z]{2}[\d]{2}$/);     //   5       XX-XX-99
        sidecodes.push(/^[\d]{2}[a-zA-Z]{2}[a-zA-Z]{2}$/);     //   6       99-XX-XX
        sidecodes.push(/^[\d]{2}[a-zA-Z]{3}[\d]{1}$/);         //   7       99-XXX-9
        sidecodes.push(/^[\d]{1}[a-zA-Z]{3}[\d]{2}$/);         //   8       9-XXX-99
        sidecodes.push(/^[a-zA-Z]{2}[\d]{3}[a-zA-Z]{1}$/);     //   9       XX-999-X
        sidecodes.push(/^[a-zA-Z]{1}[\d]{3}[a-zA-Z]{2}$/);     //   10      X-999-XX
        sidecodes.push(/^[a-zA-Z]{3}[\d]{2}[a-zA-Z]{1}$/);     //   11      XXX-99-X
        sidecodes.push(/^[a-zA-Z]{1}[\d]{2}[a-zA-Z]{3}$/);     //   12      X-99-XXX
        sidecodes.push(/^[\d]{1}[a-zA-Z]{2}[\d]{3}$/);     	  //   13      9-XX-999
        sidecodes.push(/^[\d]{3}[a-zA-Z]{2}[\d]{1}$/);         //   14      999-XX-9

        for (let i = 0; i < sidecodes.length; i++) {
            if (licensePlate.match(sidecodes[i])) {
                return i + 1;
            }
        }
        if (licensePlate.match(specialLicensePlates)) {
            return 'CD';
        }
        return false;
    },

    formatLicenseplate(licensePlate, sidecode) {

        licensePlate = licensePlate.replace(/-/g, '');

        if (sidecode !== false && sidecode <= 6) {
            return licensePlate.substr(0, 2) + '-' + licensePlate.substr(2, 2) + '-' + licensePlate.substr(4, 2)
        }
        if (sidecode == 7 || sidecode == 9) {
            return licensePlate.substr(0, 2) + '-' + licensePlate.substr(2, 3) + '-' + licensePlate.substr(5, 1)
        }
        if (sidecode == 8 || sidecode == 10) {
            return licensePlate.substr(0, 1) + '-' + licensePlate.substr(1, 3) + '-' + licensePlate.substr(4, 2)
        }
        if (sidecode == 11 || sidecode == 14) {
            return licensePlate.substr(0, 3) + '-' + licensePlate.substr(3, 2) + '-' + licensePlate.substr(5, 1)
        }
        if (sidecode == 12 || sidecode == 13) {
            return licensePlate.substr(0, 1) + '-' + licensePlate.substr(1, 2) + '-' + licensePlate.substr(3, 3)
        }
        return licensePlate
    }

}