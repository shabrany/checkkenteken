import Vue from 'vue';
import SearchLicense from './components/SearchLicense.vue';

Vue.component('search-license', SearchLicense);

new Vue({
	el: '#app'
});
