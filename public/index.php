<?php

use shabrany\Application\BasicInfoAction;
use shabrany\Application\HomeAction;
use Slim\App;

require __DIR__ . '/../vendor/autoload.php';

$container = require __DIR__ . '/../src/dic.php';

$app = new App($container);

$app->get('/', HomeAction::class);

$app->post('/search', BasicInfoAction::class);

$app->run();
